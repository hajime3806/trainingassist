package com.example.testapplication

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.testapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), MainActivityNavigator {


    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("debug","startActivity")

        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)


        val viewModel = MainActivityViewModel(this)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel

    }

//    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
//        return super.onCreateView(name, context, attrs)
//    }

    override fun navigateHome() {
        findNavController(R.id.main_nav_host).runCatching {
            navigate(R.id.HomeFragment)
        }
    }

    override fun navigateHistory() {
        findNavController(R.id.main_nav_host).runCatching {
            navigate(R.id.TrainingHistoryFragment)
        }
    }
}