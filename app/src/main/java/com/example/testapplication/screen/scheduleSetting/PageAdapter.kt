package com.example.testapplication.screen.scheduleSetting

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when (position) {
            // どのFragmentを表示するか
            0 -> {return  MenuListFragment()}
            1 -> {return  WeekMenuFragment()}
            else -> {return  MenuListFragment()}
        }
    }

    override fun getCount(): Int {
        return 2;
    }

    // スワイプビューのタイトルを決める
    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0 -> {return "MENU LIST"}
            1 -> {return  "WEEKLY MENU"}
            else -> {return null}
        }
    }
}