package com.example.testapplication.screen.trainingHistory

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.testapplication.databinding.TrainingHistoryFragmentBinding
import com.example.testapplication.model.pref

class TrainingHistoryFragment : Fragment() {

    private lateinit var binding: TrainingHistoryFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TrainingHistoryFragmentBinding.inflate(inflater, container, false)

        //preferencesのインスタンス取得
        val preferences = this.activity?.getSharedPreferences(pref.PREF_NAME, Context.MODE_PRIVATE)
        //preferences読み込み
        val countAbdominalMuscle = preferences?.getInt(pref.PREF_KEY_MENU_COUNT_ADOMINAL_MUSCLE, 0)

        binding.viewModel = TrainingHistoryViewModel(countAbdominalMuscle)
        return binding.root
    }
}