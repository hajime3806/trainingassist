package com.example.testapplication.screen.scheduleSetting

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.room.Room.databaseBuilder
import com.example.testapplication.databinding.FragmentMenuListBinding
import com.example.testapplication.model.Database
import com.example.testapplication.model.Menu
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*


class MenuListFragment : Fragment() {

    private lateinit var binding: FragmentMenuListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val context = this.activity?.applicationContext
        //コルーチン処理（ワーカースレッド）
        CoroutineScope(Dispatchers.Default).launch {
            //データベース処理
            val database =
                context?.let {
                    databaseBuilder(it, Database::class.java, "database-name")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            val menuDao = database?.menuDao()
            val newMenu = Menu(0, Date().time.toString(), Date().time.toString())
            menuDao?.insert(newMenu)

//            menuDao?.getAll().toString()　これでGet
            Log.v("hajime-", "after insert ${menuDao?.getAll().toString()}")

            menuDao?.deleteAll()
            Log.v("hajime-", "after deleteAll ${menuDao?.getAll().toString()}")
        }

        binding = FragmentMenuListBinding.inflate(inflater, container, false)
        return binding.root
    }

}