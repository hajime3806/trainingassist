package com.example.testapplication.screen.goalSetting

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.testapplication.databinding.GoalSettingFragmentBinding
import com.example.testapplication.model.pref.PREF_KEY_GOAL
import com.example.testapplication.model.pref.PREF_NAME


class GoalSettingFragment : Fragment(), GoalSettingNavigator {

    private lateinit var binding: GoalSettingFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = GoalSettingFragmentBinding.inflate(inflater, container, false)

        val preferences = this.activity?.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val savedGoalRes = preferences?.getString(PREF_KEY_GOAL,"未入力")

        binding.viewModel = GoalSettingViewModel(this@GoalSettingFragment, savedGoalRes)

        return binding.root
    }

    override fun onClickRegisterButton() {
        //preferencesのインスタンス取得
        val preferences = this.activity?.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
//        this.context?.let { getPref(it,PREF_NAME) }
        //書き込み
        val editor = preferences?.edit()
        editor?.putString(PREF_KEY_GOAL,binding.goal.text.toString())
        editor?.apply()
//        this.context?.let { putPrefString(it, PREF_NAME, PREF_KEY_GOAL, binding.goal.text.toString()) }
    }

}