package com.example.testapplication.screen.scheduleSetting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.testapplication.databinding.ScheduleSettingFragmentBinding

class scheduleSettingFragment : Fragment() {

    private lateinit var viewModel: ScheduleSettingViewModel
    private lateinit var binding: ScheduleSettingFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = ScheduleSettingFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragmentManager = (activity as FragmentActivity).supportFragmentManager
        binding.pager.adapter = PageAdapter(fragmentManager)
        binding.tabLayout.setupWithViewPager(binding.pager)

    }
}
