package com.example.testapplication.screen.goalSetting

class GoalSettingViewModel(private  val navigator: GoalSettingNavigator, val savedGoalRes: String?) {

    fun onClickRegisterButton(){
        navigator.onClickRegisterButton()
    }
}