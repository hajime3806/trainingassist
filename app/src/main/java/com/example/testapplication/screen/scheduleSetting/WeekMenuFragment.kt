package com.example.testapplication.screen.scheduleSetting

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testapplication.R
import com.example.testapplication.databinding.FragmentMenuListBinding
import com.example.testapplication.databinding.FragmentWeekMenuBinding

class WeekMenuFragment : Fragment() {

    private lateinit var binding: FragmentWeekMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWeekMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

}