package com.example.testapplication.screen.home

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.navigation.fragment.findNavController
import com.example.testapplication.HomeNavigator
import com.example.testapplication.R
import com.example.testapplication.databinding.HomeFragmentBinding
import com.example.testapplication.model.pref.PREF_KEY_GOAL
import com.example.testapplication.model.pref.PREF_KEY_MENU_COUNT_ADOMINAL_MUSCLE
import com.example.testapplication.model.pref.PREF_NAME


class HomeFragment : Fragment(), HomeNavigator {

    private lateinit var binding: HomeFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)

        //preferencesのインスタンス取得
        val preferences = this.activity?.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        //preferences読み込み
        val savedGoalRes = preferences?.getString(PREF_KEY_GOAL, "未入力")

        binding.viewModel = HomeViewModel(this@HomeFragment, savedGoalRes)
        return binding.root
    }



    //ログ

    override fun onClickScheduleSettingButton() {
        findNavController().navigate(R.id.action_homeFragment_to_scheduleSettingFragment)
    }

    //    override fun onClickGoalSettingButton() {
//        findNavController().navigate(R.id.action_homeFragment_to_goalSettingFragment)
//    }

//    override fun onClickCompletionAbdominalMuscle() {
//        //preferencesのインスタンス取得
//        val preferences = this.activity?.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
//
//        //達成回数を更新
//        val editor = preferences?.edit()
//        preferences?.let {
//            editor?.putInt(PREF_KEY_MENU_COUNT_ADOMINAL_MUSCLE,
//                it.getInt(PREF_KEY_MENU_COUNT_ADOMINAL_MUSCLE, 0) + 1)
//        }
//        editor?.apply()
//    }
}