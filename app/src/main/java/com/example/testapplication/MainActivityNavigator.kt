package com.example.testapplication

interface MainActivityNavigator {
    fun navigateHome()
    fun navigateHistory()
}