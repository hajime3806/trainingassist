package com.example.testapplication.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Menu::class), version = 1)
abstract class Database : RoomDatabase() {
    abstract fun menuDao(): MenuDao
}
