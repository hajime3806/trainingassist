package com.example.testapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey

//テーブル情報
@Entity(tableName = "menus")
data class Menu(
    @PrimaryKey(autoGenerate = false) val id: Int,
    val firstName: String?,
    val lastName: String?
)
