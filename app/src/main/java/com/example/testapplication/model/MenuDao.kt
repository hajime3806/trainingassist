package com.example.testapplication.model

import androidx.room.*

//データ操作
@Dao
interface MenuDao {
    @Insert
    fun insert(menu : Menu)
    @Update
    fun update(menu : Menu)
    @Delete
    fun delete(menu : Menu)
    @Query("delete from menus")
    fun deleteAll()
    @Query("select * from menus")
    fun getAll(): List<Menu>
    @Query("select * from menus where id = :id")
    fun getUser(id: Int): Menu
}
