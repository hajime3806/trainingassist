package com.example.testapplication.model

import android.content.Context
import android.content.SharedPreferences

//preferencesのインスタンス取得
fun getPref(context: Context, preferencesName: String): SharedPreferences = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE)

//書き込み
fun putPrefString(context: Context, preferencesName: String, key: String, value: String) = getPref(context, preferencesName).edit().putString(key, value)

object pref {
    const val PREF_NAME = "preferences"

    const val PREF_KEY_GOAL = "PREF_KEY_GOAL"
    //暫定的にメニューごとに達成した回数を管理
    const val PREF_KEY_MENU_COUNT_ADOMINAL_MUSCLE = "PREF_KEY_MENU_COUNT_ADOMINAL_MUSCLE"
}