package com.example.testapplication

import android.util.Log

class MainActivityViewModel(private val navigator: MainActivityNavigator) {

    fun onClickHomeButton() {
        navigator.navigateHome()
        Log.d("debug","onClickHomeButton()")
    }

    fun onClickHistoryButton() {
        navigator.navigateHistory()
        Log.d("debug","onClickHistoryButton()")
    }

}